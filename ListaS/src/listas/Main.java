/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

/**
 *
 * @author johan leon
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ListaS lista = new ListaS();
        lista.insertarAlInicio("Suscribete");
        lista.insertarAlInicio("Hola");
        lista.insertarAlFinal("Mi canal es: Programación Miltoniana");
        lista.eliminar(0);
        for (int i = 0; i < lista.getTamanio(); i++) {
            System.out.println(lista.get(i));
        }
        if(!lista.esVacia()){
            System.out.println("Lista Con Elementos");
        }
        
    }
    
}

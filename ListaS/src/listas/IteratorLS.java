

package listas;
 import java.util.Iterator;
 

public class IteratorLS<T> implements Iterator<T>{

    
    private Nodo<T> posicion; 
    
    
    IteratorLS(Nodo<T> pos){            
        this.posicion=pos;            
    }

    
    @Override
    public boolean hasNext(){            
        return (posicion!=null);            
    }

   
    @Override
    public T next(){            
        if(!this.hasNext()){                
        System.err.println("Error no hay mas elementos");
        return null;                
        }            
        Nodo<T> actual=posicion;
        posicion=posicion.getSig();            
        return(actual.getInfo());
    }

   
    @Override
    public void remove(){}

}